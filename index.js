// console.log("Hello World");

// https://jsonplaceholder.typicode.com/todos


// ------------------------- 3. 


/*

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())

*/



// ------------------------- 4. 


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json())
.then(json => {

let allTitles = json.map((item => {
	return item.title;
	}))
	console.log(allTitles);

});





// ------------------------- 5.



async function fetchData(){
	let result = await(fetch("https://jsonplaceholder.typicode.com/todos/1"))

	let json = await result.json();
	console.log(json);

}

fetchData();





// ------------------------- 6.


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(response => console.log("The item " + '"' + response.title + '"' + " on the list has a status of " + response.completed));




// ------------------------- 7.


fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: "false",
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))





// ------------------------- 8.


fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body:JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do list Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))





// ------------------------- 9.



fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type" : "application/json"
		},
		body:JSON.stringify({
			completed: "false",
			dateCompleted: "07/09/21",
			id: 1,
			status: "Complete",
			tiitle: "delectus aut autem",
			userId: 1 
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))





// ------------------------- 10 and 11.




fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "02/09/23"
	})
})
.then(response => response.json())
.then(json => console.log(json))





// ------------------------- 12.



fetch("https://jsonplaceholder.typicode.com/todos/1",
	{
		method: "DELETE"
	}).then(response => response.json())
.then(json => console.log(json))